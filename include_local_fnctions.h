//I2C temp data
byte _send[2];
int resetBT = 7;
int resetAT = 8;

void vcnl4010_interupted() {
  gv_interupted = true;
  //deviceState++;
}
/******************************************************************************************
  F_debugInfo
  shows debug info
******************************************************************************************/
void F_debugInfo(String & iv_section, int & iv_debugMode, String & iv_input) {
  /*
    if(iv_debugMode == 0){
      return;
    }

    if (iv_section == "setup") {
      //show info
      //mySerial.begin(9600);
      mySerial.println("Starting..");
      mySerial.println("MAC of hub: " + String(gv_hub));
      mySerial.println("SensorID: " + String(deviceID));
      //mySerial.end();
    }
    else if (iv_section == "loopStart") {
      //mySerial.write("---");
      //mySerial.print(vcnl.readProximity());
      mySerial.write("---");
      mySerial.print(analogRead(PCINT3));
      mySerial.write("---");
      gv_interupted = true;
    }
    else if (iv_section == "loopEnd") {
      mySerial.write("\n");
    }
    else if (iv_section == "LT") {
     // mySerial.write("-interupt value: ");
     // mySerial.print(iv_input);
      mySerial.write(" -LT passed- ");
    }
    else if (iv_section == "HT") {
      //mySerial.write("-interupt value: ");
      //mySerial.print(iv_input);
      mySerial.write("- -HT passed- ");
    }
    else if (iv_section == "showState") {
      //mySerial.write(" -state:");
      //mySerial.print(gv_countNew);
      //mySerial.write("- ");
    }
  */
}
/******************************************************************************************
  F_vcnl4010SetupRegisters
******************************************************************************************/

void F_vcnl4010SetupRegisters() {


  // IR LED CURRENT REGISTER #3
  //vcnl.write8(0x83, 0b00000001); //10mA (1=10mA...20=200mA)
  vcnl.write8(0x83, 0b00010100); //200mA (1=10mA...20=200mA)
  //vcnl.write8(0x83, 0b00000001); //(1=10mA...20=200mA)

  //--- set interupt treshhold
  int lv_value = vcnl.readProximity(); //do measurement
  if (lv_value < gv_threshold) {
    deviceState = 3;
    vcnl.write8(0x8A, 0b00000000);              // LOW THRESHOLD REGISTER #10
    vcnl.write8(0x8B, 0b00000000);              // LOW THRESHOLD REGISTER #11
    vcnl.write8(0x8C, gv_highByteLowValue /*highByte(gv_threshold)*/);  // HIGH THRESHOLD REGISTER #12
    vcnl.write8(0x8D, gv_lowByteLowValue /*lowByte(gv_threshold)*/);   // HIGH THRESHOLD REGISTER #13
  }
  else {
    deviceState = 1;
    vcnl.write8(0x8A, gv_highByteLowValue /*highByte(gv_threshold)*/);  // LOW THRESHOLD REGISTER #10
    vcnl.write8(0x8B, gv_lowByteLowValue /*lowByte(gv_threshold)*/);   // LOW THRESHOLD REGISTER #11
    vcnl.write8(0x8C, 0b11111111);              // HIGH THRESHOLD REGISTER #12
    vcnl.write8(0x8D, 0b11111111);              // HIGH THRESHOLD REGISTER #13
  }

  // INTERRUPT STATUS REGISTER #14 (0x8E)
  vcnl.write8(VCNL4010_INTSTAT, 0b00001010);

  //  INTERRUPT CONTROL REGISTER #9 (0x89)
  //  -- 4 consequent measurements are needed to be triggered
  //  -- interupt from proximity
  vcnl.write8(VCNL4010_INTCONTROL, 0b01000010);

  // PROXIMITY RATE REGISTER #2
  //vcnl.write8(0x82, 0b00000111); //250m/s
  vcnl.write8(0x82, 0b00000000); //2m/s

  // COMMAND REGISTER #0
  vcnl.write8(0x80, 0b00000011);

}

void SetHighThreshold (unsigned int HighThreshold) {
  unsigned char LoByte = 0, HiByte = 0;
  LoByte = (unsigned char)(HighThreshold & 0x00ff);
  HiByte = (unsigned char)((HighThreshold & 0xff00) >> 8);
  Wire.beginTransmission(VCNL4010_I2CADDR_DEFAULT);
  _send[0] = 0x8c; // VCNL40x0 High Threshold Register, Hi Byte
  _send[1] = HiByte;
  Wire.write(_send, 2); // Write 2 bytes on I2C to VCNL40x0_ADDRESS
  _send[0] = 0x8c + 1; // VCNL40x0 High Threshold Register, Lo Byte
  _send[1] = LoByte;
  Wire.write(_send, 2); // Write 2 bytes on I2C
  Wire.endTransmission();
}

void SetInterruptControl (unsigned char InterruptControl) {
  _send[0] = 0x89; // VCNL40x0 Interrupt Control register
  _send[1] = InterruptControl;
  Wire.beginTransmission(VCNL4010_I2CADDR_DEFAULT);
  Wire.write(_send, 2); // Write 2 bytes on I2C
  Wire.endTransmission();
}

/******************************************************************************************
  F_vcnl4010PinSetup
******************************************************************************************/
void F_vcnl4010Setup(int iv_int) {

  //set vcnl4010 to interupt
  pinMode(iv_int, INPUT_PULLUP);
  //mySerial.begin(9600);     //start serial
  //mySerial.setTimeout(100);
  if (! vcnl.begin()) {   //sensor not found feedback
    //mySerial.println("Sensor not found :(");
    while (1);
  }


  //mySerial.println(F("Setup start"));

  //pinMode(PCINT8, INPUT_PULLUP);
  pinMode(gv_interuptPin, INPUT_PULLUP); //Optical Sensor 1
  //  pinMode(inputPin2, INPUT_PULLUP); //Optical Sensor 2

  pinMode(resetBT, OUTPUT);
  digitalWrite(resetBT, HIGH);

  //pinMode(resetAT, OUTPUT);
  //digitalWrite(resetAT, HIGH);


  if (! vcnl.begin()) {
    //mySerial.println("Sensor not found :(");
    pinMode(resetAT, OUTPUT);
    digitalWrite(resetAT, LOW);  //call reset
  }
  vcnl.readProximity(); //laten staan om interrupt pins te resetten
  delay(3000); //give sensors time to start up.
  //mySerial.println("Found VCNL4010");

  //mySerial.println("Setup end");
  //mySerial.end();
  SetHighThreshold(2500);
  SetInterruptControl(0b00000010);
}


/******************************************************************************************
  F_vcnl4010PinSetup
  Read if the trigger is set to RISING or FALLING and switch it
******************************************************************************************/
void F_switchInterupt(Adafruit_VCNL4010 & io_vcnl, boolean &iv_open) {

  //read register, to see if the trigger is RISING or FALLING and switch it
  byte a = io_vcnl.read8(0x8E) & 3;

  if (a != 0b01) {
    iv_open = true; // lower half of treshold

    // LOW THRESHOLD REGISTER #10
    vcnl.write8(0x8A, 0b00000000);
    // LOW THRESHOLD REGISTER #11
    vcnl.write8(0x8B, 0b00000000);

    // HIGH THRESHOLD REGISTER #12
    vcnl.write8(0x8C, highByte(gv_threshold + 100));
    // HIGH THRESHOLD REGISTER #13
    vcnl.write8(0x8D, lowByte(gv_threshold + 100));

    // INTERRUPT STATUS REGISTER #14 (0x8E)
    vcnl.write8(0x8E, 0b00001011);

  }
  else {
    iv_open = false;

    // LOW THRESHOLD REGISTER #10
    vcnl.write8(0x8A, highByte(gv_threshold));
    // LOW THRESHOLD REGISTER #11
    vcnl.write8(0x8B, lowByte(gv_threshold));

    // HIGH THRESHOLD REGISTER #12
    vcnl.write8(0x8C, 0b11111111);
    // HIGH THRESHOLD REGISTER #13
    vcnl.write8(0x8D, 0b11111111);

    // INTERRUPT STATUS REGISTER #14 (0x8E)
    vcnl.write8(0x8E, 0b00001011);

  }





}
//------------------------------------------------------------------------------
// F_checkStateHTH
//------------------------------------------------------------------------------
void F_checkStateHTH(boolean iv_state) {

  if (iv_state == 0) {
    deviceState = 1; // full
  }
  else {
    deviceState = 3; // emplty
  }

  mySerial.print(F("deviceState="));
  mySerial.println(deviceState);
  F_sendDataMulti(20);

  //  mySerial.println(countOld);
  //if door switchstatus == high
}


