/*
  0-5     ID
  6-7     lowtrigger
  8-9     hightrigger
  10-21   MAC
*/


//------------------------------------------------------------------------------------------------------
/*
  F U N C T I O N
  F_readString
*/
void F_readString(char * iv_string) {
  byte lv_stringIndex = 0;
  unsigned long lv_oldTime = millis();
  while (millis() - lv_oldTime < 1) {
    if (mySerial.available()) {
      iv_string[lv_stringIndex++] = mySerial.read();
      lv_oldTime = millis();
    }
  }
  iv_string[lv_stringIndex] = 0;
}

//------------------------------------------------------------------------------------------------------
/*
  F U N C T I O N
  F_sendConnectionCommands
*/
void F_sendConnectionCommands(char * iv_serialInput) {
  pinMode(gv_interuptPin, OUTPUT); // PUT THE VALUE TO OUTPUT TO ENABLE BLUETOOTH
  digitalWrite(gv_interuptPin, LOW);
  delay(500);
  //mySerial.println(F("AT")); //Connect to calibrator
  //delay(500);
  //mySerial.println(F("AT+CON20C38FF67B21")); //Connect to calibrator

  gv_timer1 = millis();

  mySerial.print(F("AT+ROLE1"));  //10 second timeout
  delay(500);
  F_readString(iv_serialInput);
  delay(500);
  mySerial.print(iv_serialInput);  //10 second timeout
  delay(500);
  mySerial.flush(); //clear serial communication
  delay(500);
  //mySerial.flush(); //clear all serial data
  //Connect to hub
  gv_timer1 = millis();
  BTConnection = false;
  connecting = false;
  mySerial.print(F("AT+CON20C38FF67B21"));
}

//------------------------------------------------------------------------------------------------------
/*
  F U N C T I O N
*/
void F_readEeprom(char * iv_data, byte iv_start, byte iv_length) {
  byte i = 0;
  while (i < iv_length) {
    iv_data[i] = EEPROM.read(iv_start + i);
    i++;
  }
  iv_data[i] = 0;// 0 terminator
}

//------------------------------------------------------------------------------------------------------
/*
  F U N C T I O N
*/
F_writeEeprom(char * iv_data, byte iv_start, byte iv_length) {
  int i = 0; //
  int lv_startEeprom = 0;
  bool lv_stop = 0;
  while (i < iv_length) {
    if (iv_data[i + 1] != 0 && lv_stop == 0) {
      mySerial.println(iv_data[i + 1]);
      EEPROM.write(iv_start + i, iv_data[i + 1]);
    }
    else {
      lv_stop = 1;//end of string
      EEPROM.write(iv_start + i, 0);
    }
    i++;
    delay(10);//so data has time to be printed to serial
  }
}

//------------------------------------------------------------------------------------------------------
/*
  F U N C T I O N
*/
void F_processSerial(char * iv_serialData) {

  /*---help---*/
  if (strstr(iv_serialData, "HELP")) {
    mySerial.println(F("HELP gevonden"));
    mySerial.println(F("commands: -HELP")); //    = show help
    mySerial.println(F("          -SETLOW"));//  = set low triger value
    mySerial.println(F("          -SETHIGH"));// = set high triger value
    mySerial.println(F("          -SETLZ"));//   = set low triger value soap
    mySerial.println(F("          -SETHZ"));//   = set high triger value soap
    mySerial.println(F("          -INFO"));//    = show info of sensor
    mySerial.println(F("          -SETID"));//   = set id of sensor
    mySerial.println(F("          -SETMAC"));//  = set MAC of sensor to connect to
  }

  /*---setID---*/
  if (strstr(iv_serialData, "SETID")) {
    mySerial.println(F("SETID gevonden"));
    //extract value
    char* lv_data  = strchr(iv_serialData, '=');
    //put value in eeprom
    byte lv_start = 0;
    byte lv_length = 5;
    F_writeEeprom(lv_data, lv_start, lv_length);
  }

  /*---setlow---*/
  if (strstr(iv_serialData, "SETLOW")) {
    mySerial.println(F("SETLOW gevonden"));
    //extract value
    byte lv_avrValue = 10; // averaging number
    unsigned int  lv_value;
    for (int i = 0 ; i < lv_avrValue; i++) { //average x times
      lv_value = lv_value + vcnl.readProximity() / lv_avrValue; //do measurement
    }
    byte lv_low = lv_value & 0xff; //set low byte
    byte lv_high = (lv_value >> 8) & 0xff; //set high byte
    unsigned int lv_int = (lv_high << 8) | lv_low ; //convert bytes back to int
    mySerial.println(lv_int);
    EEPROM.put(6, lv_low);
    EEPROM.put(7, lv_high);
  }

  /*---sethigh---*/
  if (strstr(iv_serialData, "SETHIGH")) {
    mySerial.println(F("SETHIGH gevonden"));
    //extract value
    byte lv_avrValue = 10;
    unsigned int  lv_value;
    for (int i = 0 ; i < lv_avrValue; i++) { //average x times
      lv_value = lv_value + vcnl.readProximity() / lv_avrValue; //do measurement
    }
    byte lv_low = lv_value & 0xff;
    byte lv_high = (lv_value >> 8) & 0xff;
    unsigned int lv_int = (lv_high << 8) | lv_low ;
    mySerial.println(lv_int);
    EEPROM.put(8, lv_low);
    EEPROM.put(9, lv_high);
  }

  /*---setlowZ---*/
  if (strstr(iv_serialData, "SETLZ")) {
    mySerial.println(F("SETLZ gevonden"));
    //extract value
    byte lv_avrValue = 10;
    unsigned int  lv_value;
    for (int i = 0 ; i < lv_avrValue; i++) { //average x times
      digitalWrite(gv_interuptPin, HIGH);
      unsigned int lv_nLed = vcnl.readAmbient();
      digitalWrite(gv_interuptPin, LOW);
      unsigned int lv_wLed = vcnl.readAmbient();
      unsigned int lv_diff = lv_wLed - lv_nLed;
      lv_value = lv_value + lv_diff / lv_avrValue; //do measurement
    }
    byte lv_low = lv_value & 0xff;
    byte lv_high = (lv_value >> 8) & 0xff;
    unsigned int lv_int = (lv_high << 8) | lv_low ;
    delay(3000);//lose connection and restart
    F_sendConnectionCommands(iv_serialData);
    delay(3000);//lose connection and restart
    mySerial.println(lv_value);
    mySerial.println(lv_int);
    EEPROM.put(6, lv_low);
    EEPROM.put(7, lv_high);

  }

  /*---sethZ---*/
  if (strstr(iv_serialData, "SETHZ")) {
    mySerial.println(F("SETHZ gevonden"));
    //extract value
    byte lv_avrValue = 10;
    unsigned int  lv_value;
    for (int i = 0 ; i < lv_avrValue; i++) { //average x times
      digitalWrite(gv_interuptPin, HIGH);
      unsigned int lv_nLed = vcnl.readAmbient();
      digitalWrite(gv_interuptPin, LOW);
      unsigned int lv_wLed = vcnl.readAmbient();
      unsigned int lv_diff = lv_wLed - lv_nLed;
      lv_value = lv_value + lv_diff / lv_avrValue; //do measurement
    }
    byte lv_low = lv_value & 0xff;
    byte lv_high = (lv_value >> 8) & 0xff;
    unsigned int lv_int = (lv_high << 8) | lv_low ;
    delay(3000);//lose connection and restart
    F_sendConnectionCommands(iv_serialData);
    delay(3000);//lose connection and restart
    mySerial.println(lv_value);
    mySerial.println(lv_int);
    EEPROM.put(8, lv_low);
    EEPROM.put(9, lv_high);
  }

  /*---setmac---*/
  if (strstr(iv_serialData, "SETMAC")) {
    mySerial.println(F("SETMAC gevonden"));
    //extract value
    char* lv_data  = strchr(iv_serialData, '=');
    //put value in eeprom 10-21   MAC
    byte lv_start = 10;
    byte lv_length = 12;
    F_writeEeprom(lv_data, lv_start, lv_length);
  }

  /*---info---*/
  if (strstr(iv_serialData, "INFO")) {
    mySerial.println(F("INFO gevonden"));

    //extract value
    char lv_data[20];
    F_readEeprom(lv_data, 0, 5);
    mySerial.print(F("ID: "));
    mySerial.println(lv_data);

    mySerial.print(F("LOW: "));
    unsigned int lv_low = (EEPROM.read(7) << 8) |  EEPROM.read(6) ;
    mySerial.println(lv_low);

    mySerial.print(F("HIGH: "));
    unsigned int lv_high = (EEPROM.read(9) << 8) |  EEPROM.read(8) ;
    mySerial.println(lv_high);

    F_readEeprom(lv_data, 10, 12);
    mySerial.print(F("MAC: "));
    mySerial.println(lv_data);
  }

}

//------------------------------------------------------------------------------------------------------
/*
  F U N C T I O N
  F_connectToCalibrator_attiny
*/
void F_connectToCalibrator_attiny() {
  char lv_serialInput[30] = ""; //for calibrator

  bool lv_infinloop = 0;
  F_sendConnectionCommands(lv_serialInput);

  delay(3000); // delay to connect

  /* if connection can be made start the calibrator mode */
  unsigned long lv_timer = millis();
  //mySerial.println(F("3."));delay(50);
  //mySerial.println(lv_timer);delay(50);
  while ((millis() - lv_timer) < 5000) {
    //mySerial.print(F("4.")); delay(50);
    delay(50);
    if (mySerial.available()) {

      //read serial
      //mySerial.println(F("5."));delay(50);
      F_readString(lv_serialInput);

      //check command exactly the next command
      char lv_command[] = "OK+CONN";
      char * tester = lv_serialInput;
      bool connected_b = false;
      tester = strstr(tester, lv_command);
      while (!connected_b && tester) {
        connected_b = (tester[7] < 'A' || tester[7] > 'Z');
        tester = strstr(tester + 1, lv_command);
      }
      if (connected_b) {
        lv_infinloop = 1;
        mySerial.print(F(" !!-> "));
        mySerial.print(lv_serialInput);
        mySerial.print(F(" ("));
        mySerial.print(F("SENSORID"));
        mySerial.println(F(")"));
        exit;
      }
      else {
        mySerial.print(F(" -> "));
        mySerial.println(lv_serialInput);
      }
    }
  }

  /* stay here in this calibrator mode for infinity*/
  while (lv_infinloop == 1) {
    //char lv_serialInput[255] = "";
    lv_serialInput[0] = 0;
    if (mySerial.available()) {
      F_readString(lv_serialInput);/*read serial*/
      /*show and proces received msg*/
      if (lv_serialInput[0] != 0) { // show what is received
        mySerial.print(F("received: "));
        mySerial.println(lv_serialInput);
        F_processSerial(lv_serialInput);
      }
    }
  }

  mySerial.println(F("start main program "));
  //    digitalWrite(gv_interuptPin, HIGH); //shut down bt

}


void F_initValues(char iv_id[],
                  char iv_mac[],
                  byte * iv_lowByteLowValue,
                  byte * iv_highByteLowValue,
                  byte * iv_lowBytehighValue,
                  byte * iv_highBytehighValue ) {
  /*read eeprom to set global values*/
  F_readEeprom(iv_id , 0 , 5 );          // id
  F_readEeprom(iv_mac, 10, 12);          // mac
  *iv_lowByteLowValue   = EEPROM.read(6); // low  value low  byte
  *iv_highByteLowValue  = EEPROM.read(7); // low  value high byte
  *iv_lowBytehighValue  = EEPROM.read(8); // high value low  byte
  *iv_highBytehighValue = EEPROM.read(9); // high value high byte
}

