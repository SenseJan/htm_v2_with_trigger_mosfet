/*------------------------------------------------------------------------------*/
/* MANUAL CHANGEBLE VARAIBLES */
/*------------------------------------------------------------------------------*/
/* 1. deviceID   */
//const int deviceID = 42;

//const String deviceID = "72R"; // 6278  6370  6444  6538    12000
//int gv_threshold = 6444+20;//72R  niets 0vel  1vel  20.00%  vol
//const String deviceID = "72L"; // 3740  3828  3888  3965  9185
//int gv_threshold = 3888+20;//72L

//const String deviceID = "91R"; //5940 6045  6130  6211  12780
//int gv_threshold = 6130+20;//91R
//const String deviceID = "91L"; //6080 6185  6270  6370  13833
//int gv_threshold = 6270+20;//91L

//const String deviceID = "92R"; // 7187  7280  7350  7455  14090
//int gv_threshold = 7350+20;//
//const String deviceID = "92L"; // 6517  6622  6682  6772  12974
//int gv_threshold = 6682+20;//

//const String deviceID = "93R"; // 5666  5789  5870  5990  13582
//int gv_threshold = 5870+20;//
//const String deviceID = "93L"; // 5740  5841  5910  6054  13480
//int gv_threshold = 5910+20;//

//const String deviceID = "96R"; // 5566  5657  5740  5840  12240
//int gv_threshold = 5740+20;//
//const String deviceID = "96L"; // 5108  5222  5290  5385  12455
//int gv_threshold = 5290+20;//

//const String deviceID = "108R"; // 6930 7060  7163  7270  15700  == 4875 4966  5045  4735  12000
//int gv_threshold = 7163 + 30; //
//char deviceID[] = "108L"; //   5571 5682  5736  5830  12659
//int gv_threshold = 5736+20;//
//char gv_hub[] = "AT+CO:20C38FF67D6A"; // id = 1039

//const String deviceID = "R"; //
//int gv_threshold = +20;//
//const String deviceID = "L"; //
//int gv_threshold = +20;//




/* 2. MAC of HUB */
//String gv_hub = "AT+CO:544A1625AB27"; // if =1006
//String gv_hub = "AT+CO:20C38FF65BAE"; // id = 1038
//String gv_hub = "AT+CO:20C38FF67D6A"; // id = 1039
//String gv_hub = "AT+CO:544A1625AB27";

char deviceID[]   = "108L"; 
int gv_threshold  = 5736 + 20; //
char gv_hub[]     = "544A1625AB27";

//eeprom data
byte gv_lowByteLowValue   ;
byte gv_highByteLowValue  ;
byte gv_lowBytehighValue  ;
byte gv_highBytehighValue ;

/*------------------------------------------------------------------------------*/
/* I N C L U D E S */
/*------------------------------------------------------------------------------*/
#include <Time.h>
#include <avr/sleep.h>
#include <avr/interrupt.h>
#include <SoftwareSerial.h>
#include "Adafruit_VCNL4010.h"
SoftwareSerial mySerial(PCINT4, PCINT1); // MySerialRX, MySerialTX
#include <EEPROM.h>

#include "include_variables.h"
#include "include_fnctions.h" //functions
#include "include_local_fnctions.h"
#include "include_calibrator_small.h"

/******************************************************************************************
  local global varaibles
******************************************************************************************/


/*------------------------------------------------------------------------------*/
/* SETUP */
/*------------------------------------------------------------------------------*/
void setup() {

  mySerial.begin(9600);
  mySerial.println(F("setupstart"));
  
  //initial settings hm-10
  F_Hm10Init();

  //get data from eeprom
    F_initValues(deviceID,
                 gv_hub,
                 &gv_lowByteLowValue,
                 &gv_highByteLowValue,
                 &gv_lowBytehighValue,
                 &gv_highBytehighValue );
gv_threshold = (gv_highByteLowValue << 8)  |  gv_lowByteLowValue;

  //setup vcnl4010
  F_vcnl4010Setup(gv_interuptPin);

  //setup interupt
  //This is done in sleep
  pinMode(gv_interuptPin, INPUT_PULLUP); //Optical Sensor 1

  //setup registers
  F_vcnl4010SetupRegisters();

  //calibrator
  F_connectToCalibrator_attiny();

  // watchdog
  if (strstr(deviceID, "L")) {
    int lv_idnr = atoi(deviceID);
    mySerial.println(lv_idnr);
    randomSeed(lv_idnr); //seed ramdom with sensorid
  }
  else {
    int lv_idnr = atoi(deviceID);
    mySerial.println(lv_idnr);
    randomSeed(lv_idnr + 1421); //seed ramdom with sensorid
  }
  gv_watchdog_counter = (gv_watchdog_counter * random(1, 25)) / 100; //wait between 75-100 percent before sending again


  mySerial.println(F("setupEND"));
}



/*------------------------------------------------------------------------------*/
/* LOOP */
/*------------------------------------------------------------------------------*/
void loop() {
  F_setSoftTXPin();   //Function to reduce the power consumption.
  mySerial.begin(9600);
  MCUSR &= ~(1 << WDRF); //Clear the watch dog reset for alive update

  /* 1. FIRST STARTUP
    ad first startup run this*/
  if (gv_firstStartup == true) {
    if (gv_BtOn == 1) {
      F_firstStartup(gv_firstStartup);
    }
    gv_firstStartup  = false;
  }

  /* 2. WATCHDOG WAKE UP gv_sleepTime PASSED
     if the attiny is awoken by watchdog*/
  else if (gv_watchdog_counter >= (gv_sleepTime / 8)) {
    mySerial.println(F("update because of gv_sleepTime passed"));
    if (gv_BtOn == true) {
      F_sendDataMulti(1);
      gv_watchdog_counter = (gv_watchdog_counter * random(1, 25)) / 100; //wait between 75-100 percent before sending again
    } else {
      gv_watchdog_counter = 0;
    }
  }

  /* 3. INTERUPTED
    when the loop is started from an interupt*/
  else if (vcnl.read8(0x8E) * 0b00000011 > 0) {
    F_switchInterupt(vcnl, gv_open);
    if (gv_BtOn == true) {
      F_checkStateHTH(gv_open);//true = empty
    }
  }

  //Check if a message has not been send and the module is in sleep modes. And wakes up again.
  else if (gv_retry_send_ble > 0) { //gv_retry_send_ble = #times send - 1
    gv_watchdog_sendmsg_cntr++;
    if (gv_watchdog_sendmsg_cntr >= 2) { //Only send a message after the module wakes up 3 times from sleep.
      F_sendDataMulti(gv_retry_send_ble);
      gv_watchdog_sendmsg_cntr = 0;
    }
  }

  /*SLEEP/WATCHDOG
    go to sleep and start a watchdog*/
  mySerial.end();
  pinMode(PCINT1, INPUT); //RESET THE pinmode to reduce power consumption.
  pinMode(PCINT4, INPUT_PULLUP);
  F_setup_watchdog(9); //wake up every 8 seconds
  F_sleep(gv_interuptPin);

}




