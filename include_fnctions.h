// FUNCTIONS

//------------------------------------------------------------------------------
// F_offInterupt
//------------------------------------------------------------------------------
void F_offInterupt(boolean iv_interuptPin) {

  // turn OFF interupt on vcnl
  //  INTERRUPT CONTROL REGISTER #9 (0x89)
  vcnl.write8(VCNL4010_INTCONTROL, 0b01000000);
  
  pinMode(iv_interuptPin, OUTPUT); //Optical Sensor 1
  digitalWrite(iv_interuptPin, HIGH);



}


//------------------------------------------------------------------------------
// F_onInterupt
//------------------------------------------------------------------------------
void F_onInterupt(boolean iv_interuptPin) {
  pinMode(iv_interuptPin, INPUT_PULLUP); //Optical Sensor 1

  // turn ON interupt on vcnl
  //  INTERRUPT CONTROL REGISTER #9 (0x89)
  //  -- 4 consequent measurements are needed to be triggered
  //  -- interupt from proximity
  vcnl.write8(VCNL4010_INTCONTROL, 0b01000010);

  //dont forget to reset the interpted triggers register like this:
  // INTERRUPT STATUS REGISTER #14 (0x8E)/reset interupt
  // vcnl.write8(0x8E, 0b00001011);

}
//FUCITION TO RESET THE PIN TO OUTPUT FOR TX!!! ////
void F_setSoftTXPin() { 
  // First write, then set output. If we do this the other way around,
  // the pin would be output low for a short while before switching to
  // output high. Now, it is input with pullup for a short while, which
  // is fine. With inverse logic, either order is fine.
  digitalWrite(PCINT1, HIGH);
  pinMode(PCINT1, OUTPUT);
}


//------------------------------------------------------------------------------
// F_wakeHm10
//------------------------------------------------------------------------------
void F_wakeHm10() {
  // wake up HM-10
    F_setSoftTXPin(); // Put the Serial back on! 
    mySerial.begin(9600);
    pinMode(gv_interuptPin, OUTPUT); // PUT THE VALUE TO OUTPUT TO ENABLE BLUETOOTH
    digitalWrite(gv_interuptPin, LOW); 
    delay(500); 
}


//------------------------------------------------------------------------------
// F_inputPin
//------------------------------------------------------------------------------
void F_inputPin(int iv_inputPin) {
  pinMode(iv_inputPin, INPUT);
  digitalWrite(iv_inputPin, LOW);
}


//------------------------------------------------------------------------------
// sleep
//------------------------------------------------------------------------------
void F_sleep(int Pin1) {

  // INTERRUPT STATUS REGISTER #14 (0x8E)/reset interupt
  vcnl.write8(0x8E, 0b00001011);

  GIMSK |= _BV(PCIE);                     // Enable Pin Change Interrupts
  PCMSK |= _BV(Pin1);                     // Use Pin1 as interrupt pin

  ADCSRA &= ~_BV(ADEN);                   // ADC off
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);    // replaces above statement

  sleep_enable();                         // Sets the Sleep Enable bit in the MCUCR Register (SE BIT)
  sei();                                  // Enable interrupts

  sleep_cpu();                            // sleep

  cli();                                  // Disable interrupts
  //ONLY USE THIS COMMAND WITH INPUT PINS, NOT WITH THE UART PINS!
  PCMSK &= ~_BV(Pin1);                    // Turn off PB4 as interrupt pin

  sleep_disable();                        // Clear SE bit
  ADCSRA |= _BV(ADEN);                    // ADC on

  sei();                                  // Enable interrupts
} // sleep


//------------------------------------------------------------------------------
// wait
//------------------------------------------------------------------------------
void wait(int duration)
{
  gv_timer1 = millis();
  while (gv_timer1 + duration > millis())
  {
      
  }
  //retryBLE = true; 
}


//------------------------------------------------------------------------------
// waitForResponse
//------------------------------------------------------------------------------
boolean waitForResponse(char * check)
{
  //  receivebuffer = "";
  //char* checkS = check.c_str();
  int lv_length = strlen(check);
  //char checkS[lv_length];

  int index = 0;

  delay(500);
  while (mySerial.available())
  {
    if (check[index] == mySerial.read()) //check next character of string is equal to what is being received
      index++; //go to next character
    else
      index = 0; //start search over


    if (index == lv_length) //entire search string received! yay
    {
      return true;
    }
    if ((gv_timer1 + 10000) < millis())
    {
      return true; //10 second timout
    }

  }
  if ((gv_timer1 + 10000) < millis())
  {
    return true; //10 second timout
  }
  return false;
}


//------------------------------------------------------------------------------
// sendData
//------------------------------------------------------------------------------
boolean sendData()
{
  //mySerial.println(F("in sendData"));

  char received[30];
  byte receivedIndex = 0;
  received[0] = 0;
  gv_timer1 = millis();
  while (!waitForResponse("OK+Set:1"))
  {
    mySerial.print(F("AT+ROLE1"));  //10 second timeout
  }

  mySerial.flush(); //clear serial communication
  delay(500);
  //mySerial.flush(); //clear all serial data


  //Connect to hub
  gv_timer1 = millis();
  BTConnection = false;
  connecting = false;
  while (connecting == false)
  {

    //mySerial.print(F("AT+CO:20C38FF65AC3")); //OK+CO::AOK+CO::EOK+CONNF // SenseBeneden
    //gv_hub
    //mySerial.print(F("AT+CO:544A1625AB27")); //OK+CO::AOK+CO::EOK+CONNF //DEVELOPMENT HUB 1006
    //mySerial.print(F("AT+CO:20C38FF65BAE")); //OK+CO::AOK+CO::EOK+CONNF //vendor HUB 1038
    //mySerial.print(F("AT+CO:20C38FF67D6A")); //OK+CO::AOK+CO::EOK+CONNF //vendor HUB 1039
    //mySerial.print(F("AT+CO:20C38FF670C4")); //OK+CO::AOK+CO::EOK+CONNF //vendor HUB 1006 dev edition
    //mySerial.print(gv_hub);

    mySerial.print("AT+CO:");
    mySerial.print(gv_hub);
    //    String conn = "AT+CO:544A1625AB27";
    //    mySerial.print(conn);

    delay(3000); // KEEP A BIT LONG
    long timer2 = millis();
    received[0] = 0;
    receivedIndex = 0;
    while (mySerial.available())
    {

      char input = mySerial.read();
      received[receivedIndex] = input;
      received[receivedIndex + 1] = 0;
      receivedIndex++;

      if ((gv_timer1 + 10000) < millis())
      {
        received[0] = 0;
        receivedIndex = 0;
        connecting = true;
        BTConnection = false;
      }
    }
    if ((gv_timer1 + 10000) < millis())
    {
      received[0] = 0;
      receivedIndex = 0;
      connecting = true;
      BTConnection = false;
    }
    else if (strstr(received, "CONNF") > 0)
    {
      received[0] = 0;
      receivedIndex = 0;
      connecting = true;
      BTConnection = false;
    }
    else if (strstr(received, "CONN") > 0)
    {
      received[0] = 0;
      receivedIndex = 0;
      connecting = true;
      BTConnection = true;
    }
  }

  if (BTConnection == true)
  {


    gv_timer1 = millis();
    while (!waitForResponse("r"))
    {
      mySerial.print(F("Ready"));  //10 second timeout
    }

    mySerial.flush(); //clear all serial data
    delay(500);

    gv_timer1 = millis();
    while (!waitForResponse("Received"))
    {
      mySerial.print(deviceID);
      mySerial.print(F("="));
      mySerial.print(deviceState);
      delay(500);
    }

    mySerial.flush(); //clear all serial data
    delay(500);// keep connection alive to retreive Batt status and RSSI status

  }// BTConnection
  else //BTConnection = true
  {
    return false;
  }


  return true;
}//sendData

//------------------------------------------------------------------------------
// putBTtoSleep
//------------------------------------------------------------------------------
void putBTtoSleep()
{
  //Disable the Mosfet
  pinMode(gv_interuptPin, INPUT_PULLUP); //Put the pinmode back to Input Pullup
  mySerial.end();
  pinMode(PCINT1, INPUT); //RESET THE pinmode to reduce power consumption. 
  pinMode(PCINT4, INPUT_PULLUP); 
}

//------------------------------------------------------------------------------
// F_sendDataMulti
//------------------------------------------------------------------------------
void F_sendDataMulti(int iv_nrOfTrails ) {
     gv_retry_send_ble = iv_nrOfTrails - 1; 
    
    F_wakeHm10(); //Wake up the HM10 
    if (sendData() == true) { //succes
      putBTtoSleep();
       gv_retry_send_ble = 0; 
      return;
    }
    else {
      putBTtoSleep(); // wait before trying again
      gv_watchdog_sendmsg_cntr = 0; 
    }
  
}


//------------------------------------------------------------------------------
// firstStartUp
//------------------------------------------------------------------------------
void F_firstStartup(boolean &cv_firstStartup) {
  cv_firstStartup = false;
  F_sendDataMulti(20);

}



//------------------------------------------------------------------------------
// F_setup_watchdog
//------------------------------------------------------------------------------
//Sets the watchdog timer to wake us up, but not reset
//0=16ms, 1=32ms, 2=64ms, 3=128ms, 4=250ms, 5=500ms
//6=1sec, 7=2sec, 8=4sec, 9=8sec
//From: http://interface.khm.de/index.php/lab/experiments/sleep_watchdog_battery/
void F_setup_watchdog(int timerPrescaler) {

  if (timerPrescaler > 9 ) timerPrescaler = 9; //Limit incoming amount to legal settings

  byte bb = timerPrescaler & 7;
  if (timerPrescaler > 7) bb |= (1 << 5); //Set the special 5th bit if necessary

  //This order of commands is important and cannot be combined
  MCUSR &= ~(1 << WDRF); //Clear the watch dog reset
  WDTCR |= (1 << WDCE) | (1 << WDE); //Set WD_change enable, set WD enable
  WDTCR = bb; //Set new watchdog timeout value
  WDTCR |= _BV(WDIE); //Set the interrupt enable, this will keep unit from resetting after each int



}

/******************************************************************************************
  ISR
******************************************************************************************/
//This runs each time the watch dog wakes us up from sleep
ISR(WDT_vect) {
  gv_watchdog_counter++;
}


//------------------------------------------------------------------------------
// F_Hm10Init
//------------------------------------------------------------------------------
void F_Hm10Init() {
  pinMode(gv_interuptPin, OUTPUT); //led
  digitalWrite(gv_interuptPin, LOW); //led on
  
  delay(100);
  mySerial.print(F("AT+CLEAR"));
  delay(100);
  mySerial.print(F("AT+ERASE"));
  delay(100);
  mySerial.print(F("AT+NOTI1"));
  delay(100);
  mySerial.print(F("AT+IMME1"));
  delay(100);
  mySerial.print(F("AT+MODE2"));
  delay(100);
  //Set the LED Port TO BLINKING WHEN Powered! 
  mySerial.print(F("AT+PIO10"));
  delay(100);

}



